import React, { useContext } from 'react'
import { Modal, Form, Input, Select } from 'antd'
import Moment from 'moment'
import { ContactsContext } from '../contexts/Contacts'
import DatePicker from '../components/base/DatePicker'

export interface Contact {
  id: string;
  createdAt: string;
  name: string;
  birthday: string;
  avatar?: [];
  email?: any;
  phone?: string;
}

interface Props {
  contact: Contact;
  open: boolean;
  onClick: any;
}

const { Option } = Select

export default function EditContact({ contact, open, onClick }: Props) {
  const { isExecuting, editContact } = useContext(ContactsContext)
  const [form] = Form.useForm()

  const onFinish = (values: any) => {
    console.log('Success:', values)
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }

  const handleFormSubmit = () => {
		form.validateFields()
			.then((values) => {
        values.birthday = Moment(values.birthday).format('YYYY-MM-DD')
        delete values.prefix // TODO: Read prefix and concatenate it with phone
        editContact(contact.id, values)
        !isExecuting && onClick()
			})
			.catch((errorInfo) => {})
	}

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select defaultValue="44" style={{ width: 80 }}>
        <Option value="44">+44</Option>
      </Select>
    </Form.Item>
  )

  return (
    <>
      <Modal title="Edit a contact" visible={open} onOk={() => handleFormSubmit()} onCancel={onClick} okButtonProps={{ disabled: isExecuting ? true : false}}>
        <Form
          form={form}
          name="editContactForm"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          initialValues={{ remember: true }}
          style={{ width: '100%'}}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          size="large"
        >
            <Form.Item
              label="Full name"
              name="name"
              initialValue={contact.name}
              rules={[{ required: true, message: 'Please input your name!' }]}
            >
              <Input placeholder="Please inform at least your first and last names" size="large" />
            </Form.Item>
            <Form.Item
              label="Email"
              name="email"
              initialValue={contact.email}
              rules={[{ required: true, message: 'Please input your email!' }]}
            >
              <Input placeholder="eg. john.doe@gmail.com" size="large" />
            </Form.Item>
            <Form.Item
              label="Phone"
              name="phone"
              initialValue={contact.phone}
              rules={[{ required: true, message: 'Please input your phone!' }]}
            >
              <Input addonBefore={prefixSelector} placeholder="Phone" size="large" />
            </Form.Item>
            <Form.Item
              label="Birthday"
              name="birthday"
              initialValue={contact.birthday}
            >
              <DatePicker default={contact.birthday}/>
            </Form.Item>
            <Form.Item
              label="Avatar"
              name="avatar"
              initialValue={contact?.avatar}
            >
              <Input placeholder="This is a reference to an online avatar" size="large" />
            </Form.Item>
        </Form>
      </Modal>
    </>
  )
}
