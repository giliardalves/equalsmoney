// import AppHeader from '../components/app/Header'
import { Layout, Row, Col } from 'antd'
const { /*Header,*/ Content/*, Footer, Sider*/ } = Layout

const ClientLayout = ({ children }) => (
    <Layout>
      {/* <AppHeader /> */}
      <Content>
        <Row align='middle' justify='space-around' style={{ backgroundColor: 'transparent', padding: '50px 0px 30px 0px'}}>
          <Col span={16}>{ children }</Col>
        </Row>
      </Content>
      {/* <Footer>Footer</Footer> */}
    </Layout>
)

export default ClientLayout