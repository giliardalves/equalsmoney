const DefaultLayout = ({ children }) => (
  <div className="main-container">
    <div className="content-wrapper">{ children }</div>
  </div>
)

export default DefaultLayout