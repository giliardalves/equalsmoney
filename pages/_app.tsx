import { motion } from 'framer-motion'
import '../assets/styles/globals.css'
import '../assets/styles/variables.less'

import type { AppProps } from 'next/app'
import { ToasterProvider } from '../contexts/Toaster'
import { ContactsProvider } from '../contexts/Contacts'

// Layouts
import DefaultLayout from '../layouts/default'
// import ClientLayout from '../layouts/client'

function MyApp({ Component, pageProps, router }: AppProps) {
  // if (router.pathname.startsWith('/dashboard')) {
  //   return (
  //     <motion.div>
  //       <ToasterProvider>
  //         <ContactsProvider>
  //           <ClientLayout>
  //             <Component {...pageProps} />
  //           </ClientLayout>
  //         </ContactsProvider>
  //       </ToasterProvider>
  //     </motion.div>
  //   )
  // }

  const variants = {
    hidden: { opacity: 0, x: -200, y: 0 },
    enter: { opacity: 1, x: 0, y: 0 },
    exit: { opacity: 0, x: 0, y: -100 },
  }

  // in case none of the paths are redirected to its desired layout, then falls back to the default layout
  return (
    <motion.div
      variants={variants} // Pass the variant object into Framer Motion 
      initial="hidden" // Set the initial state to variants.hidden
      animate="enter" // Animated state to variants.enter
      exit="exit" // Exit state (used later) to variants.exit
      transition={{ type: 'linear', delay: 0.5 }} // Set the transition to linear
    >
      <ToasterProvider>
        <ContactsProvider>
          <DefaultLayout>
            <Component {...pageProps} />
          </DefaultLayout>
        </ContactsProvider>
      </ToasterProvider>
    </motion.div>
  )
}

export default MyApp
