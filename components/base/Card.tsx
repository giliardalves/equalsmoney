
import { useState, useContext } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { Card as AntCard, Avatar, Tooltip, Drawer, Typography, Row, Col, Space, Popconfirm } from 'antd'
import { EditOutlined, EyeOutlined, DeleteOutlined } from '@ant-design/icons'
import AddANewContact from '../../dialogs/AddANewContact'
import EditContact from '../../dialogs/EditContact'
import { ContactsContext } from '../../contexts/Contacts'

const { Meta } = AntCard

export interface Props {
  key: string;
  title: string;
  description?: string;
  actions?: string[];
  cover?: any;
  avatar?: string;
  to?: string;
  minHeight?: number;
  isAddCard?: boolean;
  data?: any;
}

export default function Card(props: Props) {
  const { deleteContact } = useContext(ContactsContext)

  const [isDrawerVisible, setIsDrawerVisible] = useState(false)
  const [isDialogVisible, setIsDialogVisible] = useState(false)
  const [isEditDialogVisible, setIsEditDialogVisible] = useState(false)

  const showDrawer = () => {
    setIsDrawerVisible(true)
  }

  const onClose = () => {
    setIsDrawerVisible(false)
  }

  // Considering the card is clickable and that a to has been passed down
  if (props.to) {
    return (
      <Link key={props.key} href={props.to}>
        <AntCard
          hoverable
          style={{ width: '100%', minHeight: 510 }}
          cover={ props.cover }
          size="small"
        >
          <Meta
            avatar={props.avatar && <Avatar src={props.avatar && props.avatar} />}
            title={props.title || "Unknown"}
            description={props.description || null}
          />
        </AntCard>
      </Link>
    )
  } else if (props.isAddCard) {

    const openDialog = () => {
      setIsDialogVisible(!isDialogVisible)
    }

    return (
      <>
        <AntCard
          key={props.key}
          hoverable
          style={{ borderStyle: 'dashed', borderWidth: 1, borderColor: '#f4f4f4' }}
          bodyStyle={{ minHeight: 510, margin: 0, backgroundColor: 'hsl(0, 0%, 99%)' }}
          headStyle={{ fontWeight: 200 }}
          bordered={false}
          onClick={() => openDialog()}
        >
          <div style={{ padding: 10 }}>
            <div style={{ marginTop: 110, marginBottom: 30, fontWeight: 300 }}>Add a new contact?</div>
            <Image alt="example" src={require('../../assets/media/images/add.svg')} width={80} height={80} />
          </div>
        </AntCard>
        <AddANewContact open={isDialogVisible} onClick={() => openDialog()}/>
      </>
    )
  }

  const openEditDialog = () => {
    setIsEditDialogVisible(!isDialogVisible)
  }

  //
  return (
    <>
      <AntCard
        key={props.key}
        hoverable
        style={{ width: '100%', minHeight: props.minHeight || "inherit" }}
        cover={ props.cover }
        // onClick={() =>  showDrawer()} // in case we want to make the card clickable
        actions={(props.actions && props.actions.length > 0) ? ([
          <Tooltip key={`view-${props.key}`} title="View contact details"><EyeOutlined key="view" onClick={() => showDrawer()} /></Tooltip>,
          <Tooltip key={`edit-${props.key}`} title="Edit this contact"><EditOutlined key="edit" onClick={() => {openEditDialog()}} /></Tooltip>,
          <Tooltip key={`delete-${props.key}`} title="Delete contact">
            <Popconfirm
              title="Are you sure to delete this contact?"
              onConfirm={() => deleteContact(props.data.id)}
              // onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
              <DeleteOutlined key="delete" />
            </Popconfirm>
          </Tooltip>,
        ]) : []}
      >
        <Meta
          avatar={props.avatar && <Avatar src={props.avatar && props.avatar} />}
          title={props.title || "Unknown"}
          description={props.description || null}
        />
      </AntCard>
      <Drawer key={`drawer-${props.key}`} title="Contact info" placement="right" onClose={onClose} visible={isDrawerVisible}>
        <Row align="middle" justify="start" style={{ borderStyle: 'dashed', borderWidth: 1, borderColor: '#f4f4f4', backgroundColor: '#fcfcfc', padding: 10, marginBottom: 20}}>
          {props.data && props.data.avatar && (
            <Col className="gutter-row" span={8}>
              <Avatar src={props.data.avatar && props.data.avatar} size={80} />
            </Col>
          )}
          <Col className="gutter-row" span={16}>
            <Row align="middle" justify="start">
              <Col className="gutter-row" span={24}>
                <Typography.Title level={2} style={{ color: 'RGB(224, 27, 132)'}}>{props.title}</Typography.Title>
              </Col>
              <Col className="gutter-row" span={24}>
                <Space size="large">
                  <Tooltip title="Edit this contact"><EditOutlined key="edit" onClick={() => {openEditDialog()}} /></Tooltip>
                  <Tooltip title="Delete contact">
                    <Popconfirm
                      title="Are you sure to delete this contact?"
                      onConfirm={() => deleteContact(props.data.id)}
                      // onCancel={cancel}
                      okText="Yes"
                      cancelText="No"
                    >
                      <DeleteOutlined key="delete" />
                    </Popconfirm>
                  </Tooltip>
                </Space>
              </Col>
            </Row>
            
          </Col>
        </Row>

        {props.data && (
          <>
            <Typography.Title level={5}>Created at</Typography.Title>
            <Typography.Text>{props.data.createdAt}</Typography.Text>
            <Typography.Title level={5}>Email</Typography.Title>
            <Typography.Text>{props.data.email}</Typography.Text>
            <Typography.Title level={5}>Phone</Typography.Title>
            <Typography.Text>{props.data.phone}</Typography.Text>
            <Typography.Title level={5}>Birthday</Typography.Title>
            <Typography.Text>{props.data.birthday}</Typography.Text>
          </>
        )}
      </Drawer>
      <EditContact key={`editContact-${props.key}`} open={isEditDialogVisible} onClick={() => openEditDialog()} contact={props.data}/>
    </>
  )
}