import { DatePicker as AntDatePicker } from 'antd'
import Moment from 'moment'

export interface Props {
  default?: string;
}

const dateFormat = 'YYYY-MM-DD'

export default function DatePicker(props: Props) {
  return props.default ? <AntDatePicker format={dateFormat} defaultValue={Moment(props.default, dateFormat)} style={{ width: '100%' }} /> : <AntDatePicker format={dateFormat} style={{ width: '100%' }} />
}
