import React, { useState } from 'react'
import Lottie from 'react-lottie'

export interface Props {
  cover?: string;
  size?: number;
}

export default function LottieFile(props: Props) {
  const [isStopped, setIsStopped] = useState(false)
  const [isPaused, setIsPaused] = useState(false)

  const buttonStyle = {
    display: 'block',
    margin: '10px auto'
  }

  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: require(`../../assets/media/lotties/${props.cover}.json`),
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  return (
    <>
      <Lottie
        options={defaultOptions}
        height={props.size ? props.size : 300}
        width={'70%'}
        isStopped={isStopped}
        isPaused={isPaused}
        style={{ padding: 5 }}
      />
      {/* <button style={buttonStyle} onClick={() => setIsStopped(true)}>stop</button>
      <button style={buttonStyle} onClick={() => setIsStopped(false)}>play</button>
      <button style={buttonStyle} onClick={() => setIsPaused(!isPaused)}>pause</button> */}
    </>
  )
}
