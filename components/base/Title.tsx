import { Typography as AntTypography } from 'antd'

type levelsAccepted = 1|2|3|4|5

export interface Props {
  level?: levelsAccepted;
  title: string;
  secondaryTitle?: string;
}

export default function TypographyTitle(props: Props) {
  return (
    <AntTypography.Title level={props.level ? props.level : 1}>{props.title} <a>{props.secondaryTitle}</a></AntTypography.Title>
  )
}