import { useRouter } from 'next/router'
import { Button as AntButton } from 'antd'
import { ArrowLeftOutlined } from '@ant-design/icons'

export interface Props {
  label: string;
  type?: string;
}

export default function Button(props: Props) {
  const router = useRouter()

  if (props.type === 'goBack') {
    return <AntButton type="primary" shape="round" size="large" icon={<ArrowLeftOutlined />} onClick={() => router.back()} style={{ position: 'absolute', top: '1%', left: '1%'}}>{props.label}</AntButton>
  }
  return <AntButton type="primary">{props.label}</AntButton>
}
