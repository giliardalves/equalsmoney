/** @type {import('next').NextConfig} */
const withLess = require("next-with-less")
const path = require("path")

const themeVariables = path.resolve(__dirname, './assets/styles/variables.less')

module.exports = withLess({
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/contacts/contacts-viewer',
        destination: '/contacts-list',
      },
    ]
  },
  lessLoaderOptions: {
    additionalData: (content) =>
      `${content}\n\n@import '${themeVariables}';`,
  },
})
