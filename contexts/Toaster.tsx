import React, {FC, createContext} from 'react'
import { notification } from 'antd'

const ToasterContext = createContext<any>(null)

const ToasterProvider: FC = (props) => {
  const dispatch = (message: string, description?: string, icon?: string) => {
    notification.open({
      message: message,
      description: description,
      icon: icon,
      onClick: () => {
        console.log("Notification clicked!")
      }
    })
  }

  return (
    <ToasterContext.Provider value={{ dispatch }}>
      {props.children}
    </ToasterContext.Provider>
  )
}

export {ToasterContext, ToasterProvider}