import React, {FC, createContext, useState, useEffect, useContext} from 'react'
import { ToasterContext } from '../contexts/Toaster'

const ContactsContext = createContext<any>(null)

export interface Contact {
  id: string;
  createdAt: string;
  name: string;
  birthday: string;
  avatar?: [];
  email?: any;
  phone?: string;
}[]

type AddNewContactInput = {
  name: string;
  email: string;
  phone: string;
  birthday: string;
  avatar?: string;
}

type EditNewContactInput = {
  id: string;
  name: string;
  email: string;
  phone: string;
  birthday: string;
  avatar?: string;
}

const ContactsProvider: FC = (props) => {
  const { dispatch } = useContext(ToasterContext)
  const [isExecuting, setIsExecuting] = useState<boolean>(false)
  const [contacts, setContacts] = useState<Contact[]>([])

  // fetches contacts list on mount
  useEffect(() => {
    const fetchContacts = async () => {
      fetch("https://61c32f169cfb8f0017a3e9f4.mockapi.io/api/v1/contacts", { method: 'GET' }).then(res => {
        if (res.ok) {
          return res.json()
        }
        throw res
      }).then(data => {
        setContacts(data)
      }).catch(error => {
        dispatch("Error", error)
      })
    }

    fetchContacts()
  },[])

  const createContact = (contact: AddNewContactInput) => {
    setIsExecuting(true)

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(contact)
    }

    const addContact = async () => {
      fetch("https://61c32f169cfb8f0017a3e9f4.mockapi.io/api/v1/contacts", requestOptions).then(res => {
        // if (res.ok) {
          setIsExecuting(false)
          return res.json()
        // }
        // throw res
      }).then(data => {
        setContacts(oldArray => [...oldArray, data])
        dispatch("Contact created", "You've successfully created a contact")
      }).catch(error => {
        console.error('There was an error!', error)
        dispatch("Error", error)
      })
    }

    addContact()
  }

  const editContact = (contactId: string, contact: EditNewContactInput) => {
    setIsExecuting(true)

    const requestOptions = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(contact)
    }

    const updatesContact = async () => {
      fetch(`https://61c32f169cfb8f0017a3e9f4.mockapi.io/api/v1/contacts/${contactId}`, requestOptions).then(res => {
        // if (res.ok) {
          setIsExecuting(false)
          return res.json()
        // }
        // throw res
      }).then(data => {
        setContacts(
          [...contacts.map((c, index, arr) => {
            if (c.id === contactId) {
              return arr[index] = data
            }
            return c
          })]
        )
        dispatch("Contact edited", "You've successfully edited this contact")
      }).catch(error => {
        console.error('There was an error!', error);
        dispatch("Error", error)
      })
    }

    updatesContact()
  }

  const deleteContact = (contactId: string) => {
    const remove = async () => {
      fetch(`https://61c32f169cfb8f0017a3e9f4.mockapi.io/api/v1/contacts/${contactId}`, { method: 'DELETE' }).then(res => {
        // if (res.ok) {
          return res.json()
        // }
        // throw res
      }).then(data => {
        setContacts([...contacts.filter(c => c.id !== contactId)])
        dispatch("Contact deleted", "You've successfully removed this contact")
      }).catch(error => {
        console.error('There was an error!', error);
        dispatch("Error", error)
      })
    }

    remove()
  }

  return (
    <ContactsContext.Provider value={{ isExecuting, contacts, createContact, editContact, deleteContact }}>
      {props.children}
    </ContactsContext.Provider>
  )
}

export {ContactsContext, ContactsProvider}